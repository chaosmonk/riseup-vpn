//    Copyright (C) 2020 Mason Hock <mason@masonhock.com>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
//
//


const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;
const Soup = imports.gi.Soup;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Util = imports.misc.util;
const Main = imports.ui.main;
const AggregateMenu = imports.ui.main.panel.statusArea.aggregateMenu;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;


let riseupVPN;


class RiseupVPN extends PanelMenu.SystemIndicator {
    constructor() {
        super();

        this._indicator = this._addIndicator();
	this._session = Soup.Session.new();
	this._timer = Mainloop.timeout_add_seconds(1, Lang.bind(this, this._refresh));

        AggregateMenu._indicators.insert_child_above(this.indicators, AggregateMenu._network.indicators);

	this._submenu = new PopupMenu.PopupSubMenuMenuItem("", true);
	this._submenu.icon.icon_name = "network-vpn-symbolic";
	this.menu.addMenuItem(this._submenu);

	this._toggle_menuitem = new PopupMenu.PopupMenuItem("");
	this._submenu.menu.addMenuItem(this._toggle_menuitem);

        let help_menuitem = new PopupMenu.PopupMenuItem("Help...");
	help_menuitem.connect("activate", this._help);
	this._submenu.menu.addMenuItem(help_menuitem);

        let donate_menuitem = new PopupMenu.PopupMenuItem("Donate...");
	donate_menuitem.connect("activate", this._donate);
	this._submenu.menu.addMenuItem(donate_menuitem);

        this._quit_menuitem = new PopupMenu.PopupMenuItem("Quit");
	this._quit_menuitem.connect("activate", Lang.bind(this, this._quit));
	this._submenu.menu.addMenuItem(this._quit_menuitem);

	AggregateMenu.menu.addMenuItem(this.menu, 4);

	this._refresh();
    }

    _getToken() {
        return GLib.file_get_contents("/dev/shm/bitmask-token")[1].toString();
    }

    _getStatus() {
	try {
	    let message = Soup.Message.new("GET", "http://localhost:8080/vpn/status");
	    message.request_headers.append("X-Auth-Token", this._getToken());
	    this._session.send_message(message);
	    return message.response_body.data.toString();
	} catch(err) {
	    return "none";
	}
    }

    _doAction(action) {
	try {
	    Util.spawnCommandLine('curl -H "X-Auth-Token: ' + this._getToken() + '" http://localhost:8080/vpn/' + action);
	} catch(err) {
	    return null;
	}
    }

    _start() {
	if (this._getStatus() == "none")
	    Util.spawnCommandLine("riseup-vpn -w");
	else
            this._doAction("start");
	this._refresh()
    }

    _stop() {
	this._doAction("stop");
	this._refresh()
    }

    _quit() {
	this._doAction("quit");
	this._refresh()
    }

    destroy() {
        this.indicators.destroy()
        this._submenu.destroy()
	Mainloop.source_remove(this._timer);
    }

    _refresh() {
	let state = this._getStatus();

	let icon;
        switch(state) {
            case "none":
		icon = "network-vpn-offline-symbolic";
		this._submenu.label.text = "RiseupVPN Not Running";
		break;
            case "on":
		icon = "network-vpn-symbolic";
		this._submenu.label.text = "RiseupVPN On";
		break;
            case "off":
		icon = "network-vpn-offline-symbolic";
		this._submenu.label.text = "RiseupVPN Off";
		break;
            case "starting":
		icon = "network-vpn-acquiring-symbolic";
		this._submenu.label.text = "RiseupVPN Starting";
		break;
            case "stopping":
		icon = "network-vpn-acquiring-symbolic";
		this._submenu.label.text = "RiseupVPN Stopping";
		break;
	}

	this._submenu.icon.icon_name = icon;

	if (state == "on" || state == "starting") {
	    this._toggle_menuitem.label.text = "Turn Off";
	    this._toggle_menuitem.connect("activate", Lang.bind(this, this._stop))
	} else {
	    this._toggle_menuitem.label.text = "Turn On";
	    this._toggle_menuitem.connect("activate", Lang.bind(this, this._start))
	}

	if (state == "none") {
	    this.indicators.hide();
	} else {
	    this._indicator.icon_name = icon;
	    this.indicators.show();
        }

	return true;
    }

    _help() {
        Util.spawnCommandLine("xdg-open https://riseup.net/support")
    }

    _donate() {
        Util.spawnCommandLine("xdg-open https://riseup.net/vpn/donate")
    }
}


function init() { }


function enable() {
    riseupVPN = new RiseupVPN();
}


function disable() {
    riseupVPN.destroy();
    riseupVPN = null;
}
